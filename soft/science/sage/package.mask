=dev-lisp/ecls-16.1.3*
=dev-python/sphinx-2*
=sci-mathematics/arb-2.17.0*
=sci-libs/m4ri-20200115*

=dev-python/sympy-1.7.1*
=dev-python/parso-0.8*
=dev-python/jedi-0.18*

=sci-mathematics/lrcalc-2*

=sci-mathematics/lcalc-2.0*
#=sci-mathematics/planarity-3.0.1*

#=sci-mathematics/cliquer-1.22*
